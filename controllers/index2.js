 var express = require('express');
 var router = express.Router();
 var app = express();
 var passport = require('passport');//xac thuc tai khoan
 var models = require('../models/model');
 var mongoose = require("mongoose");
 
 function Data(res){
 	var Re = new Array();
 	models.User.find({}, function(err, result){
        Re = JSON.stringify(result);
        res.render('home', {Json: Re});
    })
 }

//tim kiem theo ten
 function Search_name(val, Arr){
 	models.User.find({name: val}, function(err, users){
 		if(err) throw err;
 		Arr(users);
 	});
 }
 
 //tim kiem theo dia chi email
 function Search_email(val, callback){
 	models.User.find({email: val}, function(err, users){
 		if(err) throw err;
 		callback(users);
 	});
 }


 router.route('/').get(function(req, res)
 {
 	//tra du lieu ve cho nguoi dung
 	if(req.session.name != undefined){
    	Data(res);
    }else{
    	res.redirect('login');
    }
 }).post(function(req, res){
 	//tim kiem nguoi dung
 	var val = req.body.search;
 	var Arr = new Array();

    Search_name(val, function(Arr)//ham call back
    {//dung callback
    	if (typeof Arr !== 'undefined' && Arr.length > 0) {
   			// the array is defined and has at least one element
   			res.send(JSON.stringify(Arr));
		}else{
			var Email = new Array();
			Search_email(val, function (Email){
				res.send(JSON.stringify(Email));
			});
		}

    });

 }).put(function(req, res){
 	//sua nguoi dung
 	models.User.findByIdAndUpdate(req.body.uid, {name: req.body.rname, email: req.body.remail, password: req.body.rpass}, function(err, users){
 		if(err) throw err;
 		//console.log(users);
 		Data(res);
 	});
 }).delete(function(req, res){
 	//xoa nguoi dung
 	models.User.findByIdAndRemove(req.body.rid, function(err, users){
 		if(err) throw err;
 		res.render('home');
 	});
 })

 module.exports = router;
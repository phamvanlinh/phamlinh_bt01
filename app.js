var express = require('express');//su dung express framework
var app = express();
var bodyParser = require('body-parser');
var morgan = require('morgan');
var path = require('path');
var cookieParser = require('cookie-parser');
var session = require('express-session');
//var passport = require('passport');

var port = process.env.PORT||8080;
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({secret: 'hi', resave: true, saveUninitialized: true}));
//app.use(passport.innitialize);
//app.use(passport.session());

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

var login = require('./controllers/index');
var register = require('./controllers/index1');
var home = require('./controllers/index2');

app.use('/login', login);
app.use('/register', register);
app.use('/home', home);

app.listen(port);

console.log("Server dang chay cong: "+ port);

